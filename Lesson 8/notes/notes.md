# Урок 8

## 1. Функції

[Функції в С++ / aCode](https://acode.com.ua/urok-15-funktsiyi-i-operator-return/)

[Параметри і аргументи функцій в С++ / aCode](https://acode.com.ua/urok-16-parametry-i-argumenty-funktsij/)

## 2. Бібліотека cmath

[Standard library header &lt;cmath> - cppreference.com](https://en.cppreference.com/w/cpp/header/cmath)

```cpp
#include <cmath>

int main() {
    double a = 25;
    double b = 2;
    const double sinRes = std::sin(a); // sin(a), a в радіанах!!!
    const double asinRes = std::asin(a); // arcsin(a), a в радіанах!!!
    const double cosRes = std::cos(a); // сos(a), a в радіанах!!!
    const double acosRes = std::acos(a); // arcсos(a), a в радіанах!!!
    const double tanRes = std::tan(a); // tan(a), a в радіанах!!!
    const double atanRes = std::atan(a); // arctan(a), a в радіанах!!!

    const double absRes = std::abs(a); // модуль |a|
    const double logRes = std::log(a); // натуральний логарифм
    const double log2Res = std::log2(a); // двійковий логарифм
    const double log10Res = std::log10(a); // десятковий логарифм
    const double powRes = std::pow(a, b); // піднесення до степення (a^b)
    const double sqrtRes = std::sqrt(a); // корінь квадратний з a
    const double expRes = std::exp(b); // e(число Ейлера 2,71828) в степені b

    return 0;
}
```
