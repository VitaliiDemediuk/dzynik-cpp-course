# Урок 6

## 1 Інструкції break та continue в контексті циклів

### 1.1 break;

[break statement - cppreference.com](https://en.cppreference.com/w/cpp/language/break)

    В контексті циклів інструкція **break** використовується для їх передчасного завершення роботи:

```cpp
#include <iostream>

int main() {
    int sum = 0;
    while (true) { // нескінченний цикл
        std::cout << "Enter 0 to exit or anything else to continue: ";
        int val;
        std::cin >> val;
 
        // Виходимо з циклу, якщо користувач ввів 0
        if (val == 0) {
            break;
        }

        sum += val;
    }
 
    std::cout << "The sum of all the numbers you entered is " << sum << std::endl;
 
    return 0;
}
```

    Дана програма приймає від користувача числа, поки він не введе число 0. Як тільки було введено 0, програма виводить суму всіх чисел.

### 1.2 continue;

[continue statement - cppreference.com](https://en.cppreference.com/w/cpp/language/continue)

    **Інсрукція continue** дозволяє відразу перейти в кінець тіла циклу, пропускаючи весь код, який знаходиться під ним. Це корисно в тих випадках, коли ми передчасно хочемо завершити поточну ітерацію. На відмінно від інструкції **break**, яка завершує виконання циклу, **continue** завершує виконання ітерації!

```cpp
#include <iostream>
 
int main() {
    int n;
    std::cin >> n;
	for (int count = 0; count < n; ++count) {
		// Якщо число ділиться націло на 4, то пропускаємо весь код в даній ітерації після continue 
		if (count % 4 == 0) {
			continue; // пропускаємо все і переходимо в кінець тіла циклу
        }
 
		// Якщо число не ділиться націло на 4, то виконання коду продовжується
		std::cout << count << std::endl;
 
		// Точка виконання після оператора continue переміщується сюди
	}
 
	return 0;
}
```

    Ця програма виведе всі числа від 0(включно) до n(не включно), які не кратні 4

## 2 Неявна контекстна корвертація. Правила конвертації з типом bool

### 2.1 Конвертація числових типів в bool

[Boolean conversions - cppreference.com](https://en.cppreference.com/w/cpp/language/implicit_conversion#Boolean_conversions)

**Якщо значенням виразу є нуль, то при конвертації воно стане `false`! В іншому випадку - `true`.**

## 2.2 Контекстна конвертація

[Contextual conversions - cppreference.com](https://en.cppreference.com/w/cpp/language/implicit_conversion#Contextual_conversions)

    В мові **C++** є контексти, коли аргументом очікується вираз типу **bool**. В іншому випадку відбувається неявна конвертація, якщо це можливо. Приклади таких контекстів:

- умови в конструкціях `if`, `while`, `do - while`,` for`

- операнд логічного оператора `!`, `&&` and `||`

- перший операнд тернарного оператора `?:`;

    Контекстна конвертація є одним з джерел потенційних помилок в програмі. **Їх слід  уникати!** Один з випадків, що часто трапляється серед новачків - це використання операатора `=` замість `==`.

```cpp
#include <iostream>

int main() {
    int sum = 0;
    while (true) { // нескінченний цикл
        std::cout << "Enter 0 to exit or anything else to continue: ";
        int val;
        std::cin >> val;
 
        // Ми хотіли виконувати вихід з циклу, коли користувач ввів 0
        // Але помилково використали оператор =. Результатом виразу = є число, яке ми присвоїли змінні val
        // Отже, ми ніколи не вийдемо з циклу, оскільки ми завжди будемо присвоювати 0. А 0 конвертується в false!
        if (val = 0) {
            break;
        }

        sum += val; // sum у нас змінюватись не буде, оскільки на цьому кроці він завжди дорівнює 0
    }
 
    std::cout << "The sum of all the numbers you entered is " << sum << std::endl;
 
    return 0;
}
```

## 3 Символьний тип char

    Хоча тип **char** і відноситься до цілочисельних типів даних (і, таким чином, слідує усім їхнім правилам), робота з **char** дещо відрізняється, ніж зі звичайними цілочисельними типами. Змінна типу **char** займає 1 байт. Однак замість конвертації значення типу **char** в ціле число, воно інтерпретується як **ASCII-символ**. 

    **ASCII** (від англ. *“**A**merican **S**tandard **C**ode for **I**nformation **I**nterchange”*) — це американський стандартний код для обміну інформацією, який визначає спосіб представлення символів англійської мови (+ декілька інших) у вигляді чисел від 0 до 127. Наприклад: код букви `'а'` — 97, код букви `'b'` — 98. Символи завжди повинні знаходитися в одинарних лапках.

**Таблиця ASCII-символів**:

| **Код** | **Символ**                      | **Код** | **Символ** | **Код** | **Символ** | **Код** | **Символ**   |
| ------- | ------------------------------- | ------- | ---------- | ------- | ---------- | ------- | ------------ |
| 0       | NUL (null)                      | 32      | (space)    | 64      | @          | 96      | `            |
| 1       | SOH (start of header)           | 33      | !          | 65      | A          | 97      | a            |
| 2       | STX (start of text)             | 34      | ”          | 66      | B          | 98      | b            |
| 3       | ETX (end of text)               | 35      | #          | 67      | C          | 99      | c            |
| 4       | EOT (end of transmission)       | 36      | $          | 68      | D          | 100     | d            |
| 5       | ENQ (enquiry)                   | 37      | %          | 69      | E          | 101     | e            |
| 6       | ACK (acknowledge)               | 38      | &          | 70      | F          | 102     | f            |
| 7       | BEL (bell)                      | 39      | ’          | 71      | G          | 103     | g            |
| 8       | BS (backspace)                  | 40      | (          | 72      | H          | 104     | h            |
| 9       | HT (horizontal tab)             | 41      | )          | 73      | I          | 105     | i            |
| 10      | LF (line feed/new line)         | 42      | *          | 74      | J          | 106     | j            |
| 11      | VT (vertical tab)               | 43      | +          | 75      | K          | 107     | k            |
| 12      | FF (form feed / new page)       | 44      | ,          | 76      | L          | 108     | l            |
| 13      | CR (carriage return)            | 45      | –          | 77      | M          | 109     | m            |
| 14      | SO (shift out)                  | 46      | .          | 78      | N          | 110     | n            |
| 15      | SI (shift in)                   | 47      | /          | 79      | O          | 111     | o            |
| 16      | DLE (data link escape)          | 48      | 0          | 80      | P          | 112     | p            |
| 17      | DC1 (data control 1)            | 49      | 1          | 81      | Q          | 113     | q            |
| 18      | DC2 (data control 2)            | 50      | 2          | 82      | R          | 114     | r            |
| 19      | DC3 (data control 3)            | 51      | 3          | 83      | S          | 115     | s            |
| 20      | DC4 (data control 4)            | 52      | 4          | 84      | T          | 116     | t            |
| 21      | NAK (negative acknowledge)      | 53      | 5          | 85      | U          | 117     | u            |
| 22      | SYN (synchronous idle)          | 54      | 6          | 86      | V          | 118     | v            |
| 23      | ETB (end of transmission block) | 55      | 7          | 87      | W          | 119     | w            |
| 24      | CAN (cancel)                    | 56      | 8          | 88      | X          | 120     | x            |
| 25      | EM (end of medium)              | 57      | 9          | 89      | Y          | 121     | y            |
| 26      | SUB (substitute)                | 58      | :          | 90      | Z          | 122     | z            |
| 27      | ESC (escape)                    | 59      | ;          | 91      | [          | 123     | {            |
| 28      | FS (file separator)             | 60      | <          | 92      | \          | 124     | \|           |
| 29      | GS (group separator)            | 61      | =          | 93      | ]          | 125     | }            |
| 30      | RS (record separator)           | 62      | >          | 94      | ^          | 126     | ~            |
| 31      | US (unit separator)             | 63      | ?          | 95      | _          | 127     | DEL (delete) |

    Символи від 0 до 31 в основному використовуються для форматування виводу. Більшість з них вже застаріли.

    Символи від 32 до 127 використовуються для виведення. Це літери, цифри, знаки пунктуації, які більшість комп’ютерів використовує для відображення тексту (англійською мовою).

    Наступні два стейтменти виконують одне і те ж (присвоюють змінним типу char ціле число `97`):

```cpp
char ch1 = 97; // ініціалізація змінної типу char цілим числом 97
char ch2 = 'a'; // ініціалізація змінної типу char символом 'a' (97)
```

    Будьте уважні при використанні фактичних чисел з числами, які використовуються для представлення символів (з таблиці **ASCII**). Наступні два стейтменти виконують не одне і те ж:

```cpp
char ch = 5; // ініціалізація змінної типу char цілим числом 5
char ch = '5'; // ініціалізація змінної типу char символом '5' (53)
```

**Ввід та вивід символів**

    При виводі змінних типу **char**, об’єкт `std::cout` виводить символи замість цифр. При передачі об'єкту `std::cin` змінної типу **char**, програма буде очікувати введення символу!

```cpp
#include <iostream>
 
int main() {
    std::cout << "Input a keyboard character: ";
 
    char ch;
    std::cin >> ch;
    std::cout << ch << " has ASCII code " << static_cast<int>(ch) << std::endl;
 
    return 0;
}
```

    Приклад виконання програми:

`Input a keyboard character: q`
`q has ASCII code 113`

**Мінікалькулятор**

```cpp
#include <iostream>

int main() {
    // Ввід
    // <число1> <знак + - * /> <число2>

    // Вивід
    // Результат

    double numb1;
    double numb2;
    char sign;

    bool isValid;
    do {
        std::cin >> numb1 >> sign >> numb2;
        isValid = sign == '+' || sign == '-' || sign == '*' || sign == '/';
        if (!isValid) {
            std::cout << sign << " isn't sign" << std::endl;
        }
    } while (!isValid);

    double res;
    switch (sign) {
        case '+': res = numb1 + numb2; break;
        case '-': res = numb1 - numb2; break;
        case '*': res = numb1 * numb2; break;
        case '/': res = numb1 / numb2; break;
    }

    std::cout << res << std::endl;

    return 0;
}

```


