# Інформація:

- [Презентація](https://gitlab.com/VitaliiDemediuk/dzynik-cpp-course/-/blob/master/Lesson%200/presentation/lesson0.pdf)
- Інструкція по установленню Qt Creator та створенню нового проекту ([linux](https://gitlab.com/VitaliiDemediuk/dzynik-cpp-course/-/blob/master/Lesson%200/qt_installation_instruction/linux.md)/[windows](https://gitlab.com/VitaliiDemediuk/dzynik-cpp-course/-/blob/master/Lesson%200/qt_installation_instruction/windows.md))