# Інструкція по установленню середовища програмування Qt Creator та компілятора на операційній системі Linux (Ubuntu). Створення нового проекту.

## Установлення

1. Перейдіть за посиланням https://login.qt.io/register та створити новий Qt акаунт
2. Перейдіть за цим посиланням https://www.qt.io/download-qt-installer та завантажте установник

![download](resources/download.png)

3. Знайдіть завантажений файл. Натисніть правою кнопкою миші на нього та перейдіть у властивості(Properties).

![select-properties](resources/select-properties.png)

4. Поставте галочку "Allow executing file as program" та закрийте це вікно

![select-properties](resources/set-permission.png)

5. Подвійним кліком на файл запустіть інсталятор. Введіть логін та пароль від створеного акаунта. Натискаємо кнопку **Next**

![log-in](resources/log-in.png)

6. У розділі *Open Source Obligations* виставіть всі галочки та натисніть на кнопку **Next**.

7. У розділі *Setup - Qt* просто натисніть на кнопку **Next**

8. У розділі *Contribute to Qt Developer* виберіть будь-який пункт за бажанням. Я завжди вибираю *Disable sending pseudonymous usage statistics in Qt Creator*. Натискаємо **Next**

9. У розділі *Installation Folder* я залишаю все замовчуванням. За бажанням можна змінити шлях для установки.

10. У розділі *Select Components* виставіть такі компоненти. Після чого натисніть **Next**

![components](resources/components.png)

11. Далі натисніть **Next** та виставте галочки, де це потрібно. Зачекайте закінчення установки.

------

## Створення проекту

1. Спочатку потрібно запустити **Qt Creator**

2. Далі натисніть на кнопку **Create Project**

![create project button](resources/create_prj_btn.png)

3. Далі відкриється нове вікно. Виберіть *Plain C++ Application* та натисніть кнопку **Choose...**

![project type selection](resources/prj_type_selection.png)

4. На наступному кроці придумайте ім'я проекту та виберіть розташування на комп'ютері. Після чого натисніть **Next**

![prj_name_path](resources/prj_name_path.png)

5. На кроці *Build System* виберіть **CMake**. Після чого натисніть **Next**

![build system](resources/build_system.png)

6. На кроці *Kits* виберіть **Desktop Qt 5.15.2 GCC 64bit**. Після чого натисніть **Next**

![Kits](resources/kits.png)

7. Після чого натискаємо на **Finish**. Вітаю, ваш перший проект створено!
8. Можете побачити, що при створенні проекту одразу згенерувався main.cpp файл з кодом програми **Hello, World!**
9. Щоб скомпілювати та запустити нашу програму, натискаємо на зелений трикутник в лівому нижньому куті. Результат виконання можете побачити знизу