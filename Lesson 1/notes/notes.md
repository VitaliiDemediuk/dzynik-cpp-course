# Урок 1. Структура програм на C++. Типип даних, змінні та арифметичні операції. Введення користувацьких даних у програму

## 1. Структура програм

### 1.1 Інструкції

    Програма на С++ складається із набору **інструкцій** (statement)(statement), які послідовно одна за одною виконуються. Кожна інструкція виконує певну дію. **Наприкінці інструкції у мові C++ ставиться крапка з комою (;).** Цей знак вказує компілятор на завершення інструкції. Наприклад:

```cpp
std::cout << "Hello World!";
```

    Цей рядок виводить на консоль рядок "Hello world!". Це інструкція і тому завершується крапкою з комою. Переглянемо ще декілька прикладів інструкцій:

```cpp
int x;
x = 5;
std::cout << x;
```

    `int х` — це **інстукція оголошення** . Вона повідомляє компілятору, що `х` є змінною. У програмуванні кожна змінна має свою комірку в пам’яті. Всі змінні в програмі перед їх фактичним використанням, повинні бути оголошені. Ми детально поговоримо про змінні на наступних уроках.

    `х = 5` — це **інстукція присвоювання**. Тут ми присвоюємо значення `5` змінній `х`.

    `std::cout << x;` — це **інструкція виводу**. Ми виводимо значення змінної `х` на екран.

### 1.2 Бібліотеки

    **Бібліотека** — це набір скомпільованого коду (наприклад, функцій), який був “упакований” для повторного використання в інших програмах. За допомогою бібліотек можна розширити функціонал програм. Наприклад, якщо ви пишете гру, то вам доведеться підключити бібліотеку звуку або графіки (якщо ви самостійно не хочете їх писати з нуля).

    Мова **C++** не така вже й велика, як ви могли б подумати. Проте, вона поставляється в комплекті зі **Стандартною бібліотекою С++**, яка надає додатковий функціонал. Однією з найбільш часто використовуваних частин Стандартної бібліотеки **C++** є **бібліотека iostream**, яка дозволяє виводити інформацію на екран і опрацьовувати дані, які вводить користувач.

### 1.3 Функція main

    Кожна програма на **C++** повинна мати хочаб одну функції — функція `main`. Саме з неї починається виконання програми. Визначення функції main починається з типу, що повертається. Функція main у будь-якому разі має повертати число. Коли програма завершує своє виконання, функція main() передає значення, яке вказує на результат виконання програми (успішно чи ні), назад в операційну систему. Так історично склалося, що у разі успішного виконання програми функція `main` повертає число `0`. Будь-які ненульові числа використовуються для того, щоб вказати, що щось пішло не так. Функція є блоком коду, тому її тіло обрамляється фігурними дужками, між якими визначається набір інструкцій. В майбутньому у нас ще буде виділений окремий урок про функції!

### 1.4 Приклади простих прогам на C++

    Почнемо з найпростішої програми на **C++**.

```cpp
int main()
{
    return 0;
}
```

    Ця програма має функцію `main`, яка повертає ціле число. Тому вона є коректною програмою на **C++**. Але що вона робить? Насправді нічого:) Після запуску вона одразу завершить своє виконання. Давайте детально її розберемо. У **першому** рядку бачимо оголошення функції `main`. У **другому** та **четвертому** рядку вказуємо компілятору область функції `main`. Все, що знаходиться між відкриваючою та закриваючою фігурною дужкою — вважається частиною функції `main`. У **третьому** рядку можемо побачити інструкцію `return`. Дана інструкція завершує виконання функції та повертає число `0`, що означає успішне завершення програми. 

    Тепер прийшов час написати складнішу програму. Почнемо з класичної першої програми при вивченні будь-якої мови програмування - програми яка виводить рядок *"Hello, world!*. 

```cpp
#include <iostream>

int main()
{
   std::cout << "Hello, world!";
   return 0;
}
```

**Рядок №1**: Спеціальний тип інструкції, який називається **директивою препроцесора**. Директиви препроцесора повідомляють компілятору, що йому потрібно виконати певне завдання. В цьому випадку ми повідомляємо компілятору, що хотіли б підключити вміст заголовкового файлу iostream до нашої програми. Заголовковий файл iostream дозволяє нам отримати доступ до функціоналу бібліотеки iostream для можливості виведення інформації на екран.

**Рядок №2**: Порожній простір, який ігнорується компілятором.

**Рядок №3**: Оголошення головної функції `main`.

**Рядок №4 і** **№7**: Вказуємо компілятору область функції `main`.

**Рядок №5**: Наша перша інструкція (**яка закінчується крапкою з комою**) — інструкція виводу. `std::cout` — це спеціальний об’єкт за допомогою якого ми можемо виводити дані на екран. `<<` — це оператор виводу. Все, що ми відправляємо в std::cout, — виводиться на екран. Тут ми виводимо текст `"Hello, world!"`.

**Рядок №6**: Інструкція return. 

### 1.5 Коментарі

    **Коментар** — це рядок (чи декілька рядків) тексту, який вставляється у вихідний код для пояснення того, що виконує цей код. Програміст може залишати їх, щоб його колегам чи через деякий час й самому програмісту було легше в ньому розібратися. Це вважається хорошою практикою в програмуванні. **Коментарі потрібно залишати тільки у справді незрозумілих місцях, а не загромаджувати код ними!** При компіляції коментарі ігноруються і не впливають на роботу програми та її розмір. В **C++** є два типи коментарів: **однорядкові** та **багаторядкові**.

    **Однорядкові** коментарі — це коментарі, які пишуться після символів `//`. Вони розміщуються в окремих рядках і все, що знаходиться після цих символів коментування, — ігнорується компілятором.

```cpp
#include <iostream>               // підключаємо бібліотеку iostream

int main()                        // визначаємо функцію main
{                                 // початок функції
    std::cout << "Hello World!";  // виводимо рядок на консоль
    return 0;                     // виходимо з функції
}                                 // кінець функції
```

    **Багаторядкові** коментарі — це коментарі, які пишуться між символами `/* */`. Все, що знаходиться між зірочками, — ігнорується компілятором

```cpp
#include <iostream>
/*
    Визначення функції main
    Програма виводить на консоль рядок Hello World!
*/
int main()
{
    std::cout << "Hello World!"; // виводимо рядок на консоль
    return 0;
}
```

### 1.6 Синтаксис та синтаксичні помилки

    Як ви, мабуть, знаєте, в українській мові всі речення слідують правилам граматики. Наприклад, кожне речення повинно закінчуватися крапкою. Правила, які регулюють побудову речень, називаються **синтаксисом**. Якщо ви не поставили крапку і записали два речення підряд, то це є порушенням синтаксису української мови.

    C++ також має свій синтаксис: правила написання коду/програм. При компіляції вашої програми, компілятор перевіряє код вашої програми на відповідність правилам синтаксису мови C++. Якщо ви порушили правила, то компілятор лаятиметься і видасть вам помилку.

    Наприклад, давайте подивимось, що відбудеться, якщо ми не вкажемо крапку з комою в кінці стейтменту:    

```cpp
#include <iostream>

int main()
{
   std::cout << "Hello world!"
   return 0;
}
```

Вивід компілятора:

`5:32: error: expected ‘;’ before ‘return’`

    В даному випадку компілятор нам вказав, що ми забули поставити крапку з комою у 5 рядку на місці 32 символа. У більшості випадків компілятор правильно визначає рядок з помилкою, але є ситуації, коли помилка не помітна аж до початку наступного рядка. Синтаксичні помилки нерідко відбуваються при написанні програм. На щастя, більшість з них можна легко знайти і виправити. **Але слід пам’ятати, що програма може бути повністю скомпільована і виконана тільки при відсутності помилок.**

---

## 2 Змінні, типи даних та арифметичні операції

### 2.1 Змінні

##### 2.1.1 Поняття змінної

    Програмуючи на мові C++, ми створюємо, опрацьовуємо і знищуємо об’єкти. **Об’єкт** — це частина пам’яті, яка може зберігати певне значення. В якості аналогії можна привести поштову скриньку, де ми зберігаємо інформацію і звідки її дістаємо. Всі комп’ютери мають **оперативну пам’ять**, яку використовують програми. При створенні об’єкта частина оперативної пам’яті виділяється для цього об’єкта. Більшість об’єктів, з якими ми будемо працювати в C++, є змінними. Змінні мають **тип**, **ім'я** та **значення**. Тип визначає, яку інформацію може зберігати змінна.

##### 2.1.2 Оголошення

    Перед використанням будь-яку змінну необхідно оголосити. Синтаксис інструкції оголошення змінної:

```
тип_змінної ім'я_змінної;
```

    Приклад оголошення цілочисельної змінної `x` (яка може містити тільки цілі числа):

```cpp
int x;
```

    При виконанні цієї інструкції центральним процесором частина оперативної пам’яті виділяється цьому об’єкту. Наприклад, припустимо, що для змінної `x` присвоюється комірка пам’яті під номером 150. Коли програма бачить змінну `x` в будь-якому виразі чи стейтменті, то вона розуміє, що для того, щоб отримати значення цієї змінної, потрібно заглянути в комірку пам’яті під номером 150.

##### 2.1.3 Правила іменування змінних

    У **C++**, як і в багатьох інших мовах програмування, вітається призначення змінним осмислених імен! Якщо змінна представляє вартість поїздки, то для неї слід вибрати таке ім'я як `cost_of_trip` або `costOfTrip`, але не `х` або `cot`. У **C++** необхідно дотримуватися наступних простих правил іменування:

- в іменах дозволено використання тільки символів латинського алфавіту, цифр та знак підкреслення (`_`)

- першим символом іменні не повинна бути цифра

- символи у верхньому та нижньому регістрі розглядаються як різні

- ключові слова не можна використовувати як ім'я змінної

    Приведемо декілька прикладів допустимих та недопустимих імен змінних:

```cpp
int poodle;     // допустиме ім'я
int Poodle;     // допустиме ім'я, відрізняється від poodle!
int POODLE;     // допустиме ім'я, відрізняється від двох попередніх!
int 4ever;      // недопустиме ім'я, починається з цифри
int my_stars3;  // допустиме ім'я
int _stars3;    // допустиме ім'я
int double;     // недопустиме ім'я, double - ключове слово!
int the_very_best_variable_i_can_be_version_112; // допустиме ім'я
int honky-tonk; // недопустиме ім'я, дефіси заборонені!
```

    **Список ключових слів:**  [C++ keywords - cppreference.com](https://en.cppreference.com/w/cpp/keyword)

##### 2.1.4 Присвоєння значень змінній

    Однією з найбільш поширених операцій зі змінними є операція присвоювання. **Присвоювати значання змінній ви може тільки після її оголошення.** До цього моменту програмі нічого не відомо про неї. Приклади:

```cpp
x = 8;
```

```cpp
int y;
y = 10;
x = y;
```

##### 2.1.5 Ініціалізація. Різниця між ініціалізацією та присвоюванням.

    В **C++** є дві схожі концепції, які початківці дуже часто плутають: **присвоювання** і **ініціалізація**. Після оголошення змінної, їй можна присвоїти значення за допомогою оператора присвоювання (знак `=`):

```cpp
int a; // це оголошення змінної
a = 8; // а це присвоювання змінній a значення 8
```

    В **C++** ви можете оголосити змінну і відразу присвоїти їй значення. Це називається **ініціалізацією змінної**

```cpp
int a = 8; // ініціалізуємо змінну a значенням 8
```

    Хоча ці два поняття близькі за своєю суттю і часто можуть використовуватися для досягнення одних і тих же цілей, все ж в деяких випадках слід використовувати ініціалізацію, замість присвоювання, а в деяких — присвоювання замість ініціалізації.

    **Правило: Якщо у вас відразу є значення для змінної, то використовуйте ініціалізацію, замість присвоювання.**

### 2.2 Основні(фундаментальні) типи даних

[Fundamental types - cppreference.com](https://en.cppreference.com/w/cpp/language/types)

- **int** — целочисленный тип данных.
- **float** — тип данных с плавающей запятой.
- **double** — тип данных с плавающей запятой двойной точности.
- **char** — символьный тип данных.
- **bool** — логический тип данных.

### 2.3 Літерали

    Літерал — деяке постійне значення певного типу даних, записане у вихідному коді комп'ютерної програми. Приклади літералів у мові C++:

```cpp
int x = 5;         // 5 - літерал типу int
std::cout << 3.14; // 3.14 - літерал типу double
bool x = false;    // false - літерал типу bool
std::cout << 'a';  // 'a' - літерал типу char
```

    Числові літерали можуть мати суфікси, які визначають їх типи. Ці суфікси не є обов’язковими, так як компілятор розуміє з контексту, константу якого типу даних ви хочете використовувати.

| **Тип даних** | **Суфікс**                               | **Значення**       |
| ------------- | ---------------------------------------- | ------------------ |
| int           | u чи U                                   | unsigned int       |
| int           | l чи L                                   | long               |
| int           | ul, uL, Ul, UL, lu, lU, Lu чи LU         | unsigned long      |
| int           | ll чи LL                                 | long long          |
| int           | ull, uLL, Ull, ULL, llu, llU, LLu чи LLU | unsigned long long |
| double        | f чи F                                   | float              |
| double        | l чи L                                   | long double        |

```cpp
long long x = 5ll;   // 5 - літерал типу long long
std::cout << 3.14f;      // 3.14 - літерал типу float
```

#### 2.4 Константи

    До цього моменту, всі змінні, які ми розглядали, були *неконстантними*. Їх значення можна було змінити в будь-який час, наприклад:

```cpp
int x = 5; // ініціалізуємо змінну x значенням 5
x = 10;    // змінюємо значення x на 10
```

    Візьмемо, наприклад, величину сили тяжіння на Землі: `9.8м/с^2`. Вона навряд чи зміниться найближчим часом. Використовувати константу в цьому випадку буде найкращим варіантом, тому що ми запобіжимо, таким чином, будь-яку (навіть випадкову) зміну цього значення.

    Щоб зробити змінну константою, використовуйте **ключове слово** **const** перед типом змінної або після нього, наприклад:

```cpp
const double gravity = 9.8;  // краще використовувати const перед типом даних
int const sidesInSquare = 4; // ок, але варіант вище є кращим
```

    Незважаючи на те, що мова **C++** дозволяє розміщувати `const` як перед типом даних, так і після нього, хорошою практикою вважається розміщувати `const` *перед* типом даних. Константи повинні бути ініціалізовані при оголошенні. Змінити їх значення за допомогою операції присвоювання не можна:

```cpp
const double gravity = 9.8;
gravity = 9.9; // не допускається - помилка компіляції
```

    Оголошення константи без її ініціалізації також викличе помилку компіляції:

```cpp
const double gravity; // помилка компіляції, константа повинна бути ініціалізована
```

    **Зверніть увагу**, константи можуть бути ініціалізовані за допомогою неконстантних значень:

```cpp
int age;
age = 15;
const int usersAge = age;
```

### 2.5 Вирази

    **Вирази** — це поєднання операторів, літералів та змінних. Вирази використовуються для проведення обчислень, деяких дій тощо. Вирази, що використовуються для проведення обчислень генерують певні значення. Вони можуть бути як одним значенням (наприклад, `2` чи `х`), так і комбінацією значень (наприклад, `2 + 3`, `2 + х`, `х + у` чи `(2 + х) * (y - 3)`). Наприклад, `х = 2 + 3;` — це коректна інструкція присвоювання. Вираз `2 + 3` генерує результат: значення `5`, яке потім присвоюється змінній `х`.

### 2.6 Основні операції

##### 2.6.1 Арифметичні операції

    Арифметичні операції виконуються над числами.

- Оператор `+`. Операція додавання повертає суму двох чисел:
  
  ```cpp
  int a = 10;
  int b = 7;
  int c = a + b;  // 17
  int d = 4 + b;  // 11
  ```

- Оператор `-`. Операція віднімання повертає різницю двох чисел:
  
  ```cpp
  int a = 10;
  int b = 7;
  int c = a - b;  // 3
  int d = 41 - b; // 34
  ```

- Оператор `*`. Операція множення повертає добуток двох чисел:
  
  ```cpp
  int a = 10;
  int b = 7;
  int c = a * b;  // 70
  int d = b * 5;  // 35
  ```

- Оператор `/`. Операція ділення повертає частку двох чисел:
  
  ```cpp
  int a = 20;
  int b = 5;
  int c = a / b;  // 4
  double d = 22.5 / 4.5;  // 5
  ```
  
  При діленні варто бути уважним, тому що якщо в операції беруть участь два цілих числа, то результат цього оператора буде округлятися до цілого числа, навіть якщо результат присвоюється змінній float або double:
  
  ```cpp
  double k = 10 / 4; // 2
  std::cout << k;
  ```
  
  Щоб результат представляв число з плаваючою точкою, один з операнда також повинен представляти число з плаваючою точкою:
  
  ```cpp
  double k = 10.0 / 4;     // 2.5
  std::cout << k;
  ```

- Оператор `%`. Операція отримання остачі від ділення:
  
  Остачею від ділення націло числа `m` натуральне число `n` — таке ціле число `p`($ 0 \leq p < n $), для якого справджується рівність `m = kn + p`. `m` - ділене, n - дільник, `k` - частка, `p` - остача.
  
  ```
  132:5=26(2ост.)
  133:5=26(3ост.)
  134:5=26(4ост.)
  135:5=27(0ост.)
  136:5=27(1ост.)
  ```
  
  Тепер приклад на мові **C++**
  
  ```cpp
  int a = 33;
  int b = 5;
  int c = a % b;  // 3
  int d = 22 % 4; // 2 (22 - 4*5 = 2)
  ```

##### 2.6.2 Оператори порівняння

    У мові програмування **C++** є 6 операторів порівняння:

| **Оператор**    | **Символ** | **Приклад** | **Операція**                                                   |
| --------------- | ---------- | ----------- | -------------------------------------------------------------- |
| Більше          | >          | x > y       | true, якщо x більше y, в протилежному випадку — false          |
| Менше           | <          | x < y       | true, якщо x менше y, в протилежному випадку — false           |
| Більше/Дорівнює | >=         | x >= y      | true, якщо x більше/дорівнює y, в протилежному випадку — false |
| Менше/Дорівнює  | <=         | x <= y      | true, якщо x менше/дорівнює y, в протилежному випадку — false  |
| Дорівнює        | ==         | x == y      | true, якщо x дорівнює y, в протилежному випадку — false        |
| Не дорівнює     | !=         | x != y      | true, якщо x не дорівнює y, в протилежному випадку — false     |

**Кожен з цих операторів обчислюється в логічне значення`true` (`1`) або `false` (`0`).**

---

## 3. Введення та виведення користувацьких даних у програму. Об’єкти std::cout, std::cin та std::endl

### 3.1 Об’єкт std::cout

    Як ми вже говорили на попередніх уроках, об’єкт `std::cout` (який знаходиться в **бібліотеці iostream**) використовується для виведення інформації на екран (в консольне вікно). Наприклад, згадаємо нашу програму *«Hello, world!»*:

```cpp
#include <iostream>

int main()
{
    std::cout << "Hello, world!";
    return 0;
}
```

    Для виведення декількох об’єктів в одному рядку, **оператор виводу** `<<` потрібно використати декілька разів, наприклад:

```cpp
#include <iostream>

int main()
{
   int a = 7;
   std::cout << "a is " << a;
   return 0;
}
```

    Результат виконання програми:

`a is 7`

    А який результат виконання наступної програми?

```cpp
#include <iostream>

int main()
{
   std::cout << "Hi!";
   std::cout << "My name is Anton.";
   return 0;
}
```

    Можливо, ви здивуєтесь, але:

`Hi!My name is Anton.`

### 3.2 Об’єкт std::endl

    Якщо текст потрібно вивести окремо (на декількох рядках), то тоді слід використовувати `std::endl`. При використанні з `std::cout`, `std::endl` вставляє символ нового рядка. Таким чином, ми перейдемо до початку наступного рядка, наприклад:

```cpp
#include <iostream>

int main()
{
   std::cout << "Hi!" << std::endl;
   std::cout << "My name is Anton." << std::endl;
   return 0;
}
```

Результат:

```
Hi!
My name is Anton.
```

### 3.3 Об’єкт std::cin

    `std::cin` є протилежністю `std::cout`. У той час як `std::cout` виводить дані в консоль за допомогою оператора виводу `<<`, `std::cin` отримує дані від користувача програми за допомогою **оператора вводу** `>>`. Використовуючи `std::cin` ми можемо отримувати і опрацьовувати користувацький ввід, наприклад:

```cpp
#include <iostream>

int main()
{
   std::cout << "Enter a number: "; // просимо користувача ввести будь-яке число
   int a = 0;
   std::cin >> a; // отримуємо число від користувача і зберігаємо його в змінній a
   std::cout << "You entered " << a << std::endl;
   return 0;
}
```

    Спробуйте скомпілювати і запустити цю програму. При запуску ви побачите `Enter a number:`, а потім програма чекатиме, поки ви введете число. Як тільки ви це зробите і натиснете *Enter*, програма виведе `You entered`, а потім ваше число.

    Наприклад, я ввів `7`:  

`Enter a number: 7   You entered 7`  

    Це найпростіший спосіб отримання даних від користувача. Ми будемо його використовувати в подальших прикладах.

### 3.4 Як запам'ятати?

    Початківці часто плутають `std::cin`, `std::cout`, `<<` і `>>`. Ось прості способи навчитися розрізняти ці об’єкти:

- `std::cin` і `std::cout` завжди знаходяться в лівій частині інструкції;

- `std::cout` використовується для виведення значення (c**OUT** — вивід);

- `std::cin` використовується для отримання значення (c**IN** — ввід);

- оператор виводу `<<` використовується з `std::cout` і вказує напрям, в якому дані рухаються в консоль: `std::cout << 7;` — значення `7` переміщується в консоль;

- оператор вводу `>>` використовується з `std::cin` і вказує напрям, в якому дані рухаються з консолі в змінну: `std::cin >> a;` — значення з консолі переміщується в змінну `a`.

---

## 4. Приклади програм до цього уроку

### 4.1 Hello, world!

```cpp
#include <iostream>

int main()
{
    std::cout << "Hello, world!" << std::endl;
    return 0;
}
```

### 4.2 Дуже простий калькулятор

    Написати програму, яка отримує від користувача два цілих числа та виводить на консоль їхню суму.

**Перший варіант:**

```cpp
#include <iostream>

int main()
{
    int first_numb;
    std::cin >> first_numb;
    int second_numb;
    std::cin >> second_numb;
    const int result = first_numb + second_numb;
    std::cout << result;
    return 0;
}
```

**Другий варіант:**

```cpp
#include <iostream>

int main()
{
    int first_numb, second_numb; // одразу оголошуємо дві змінні
    std::cin >> first_numb >> second_numb; // одразу зчитуємо два числа
    std::cout << first_numb + second_numb; // одразу виводимо результат виразу 
    return 0;
}
```

    **Попробуйте самостійно змінити програму, щоб вона працювала з дробовими значеннями**

### 4.3 Проста прогама

    Програма зчитує двоцифрове число і виводить через пропуск кожну цифру окремо.

#### Вхідні дані

        Натуральне число на проміжку від 10 до 99 включно.

#### Вихідні дані

        Спочатку першу цифру числа і через пропуск другу.

    Першу цифру можемо отримати за допомогою цілочисельного ділення на 10

```cpp
int a = 49 / 10; // 4
int b = 10 / 10; // 1
int c = 81 / 10; // 8
```

    Другу цифру можемо отримати за допомогою ділення з остачею на 10 

```cpp
int a = 49 % 10; // 9
int b = 10 % 10; // 0
int c = 81 % 10; // 1
```

Наведемо декілька можливих розв'язків цієї задачі

```cpp
#include <iostream>

int main()
{
    int numb;
    std::cin >> numb;
    std::cout << numb / 10 << " " << numb % 10 << std::endl;
    return 0;
}
```

```cpp
#include <iostream>

int main()
{
    int numb;
    std::cin >> numb;
    const int first_digit = numb / 10;
    const int second_digit = numb % 10;
    std::cout << first_digit << " " << second_digit << std::endl;
    return 0;
}
```

### 4.4 Арифметична прогресія

[Арифметична прогресія — Вікіпедія](https://uk.wikipedia.org/wiki/%D0%90%D1%80%D0%B8%D1%84%D0%BC%D0%B5%D1%82%D0%B8%D1%87%D0%BD%D0%B0_%D0%BF%D1%80%D0%BE%D0%B3%D1%80%D0%B5%D1%81%D1%96%D1%8F)

    Данно перший член та різницю арифметичної прогресії. Знайти n-ий член прогресії та суму пеших n членів прогресії.

Розв'язки

```cpp
#include <iostream>

int main()
{
    double a1, d; // Оголошуємо змінні для першого члену та різниці прогресії
    int n;
    cin >> a1 >> d >> n; // Зчитуємо перший член прогресії, різницю, та число n
    const double an = a1 + d*(n-1);
    const double sum = ((a1 + an) / 2) * n;
    std::cout << "a_n = " << an << std::endl;
    std::cout << "Sum = " << sum << std::endl;
    return 0;
}
```
